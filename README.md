# Links

## General
* [BootGuard](https://trmm.net/bootguard)
* [33C3 talk transcript on Heads, by Trammell Hudson](https://trmm.net/Heads_33c3)
* [FirmwareSecurity.com](https://firmwaresecurity.com/)
* [UEFI acronyms on wiki.phoenix.com](http://wiki.phoenix.com/wiki/index.php/Acronyms)
* [LinuxBoot on OCP - hackaton FB/Google/Horizon/TwoSigma](https://trmm.net/OCP)

## Introductory

* [UEFI boot how does that actually work](https://www.happyassassin.net/2014/01/25/uefi-boot-how-does-that-actually-work-then/)

## Intel ME
* [me-tools from Igor Skochinsky](https://github.com/skochinsky/me-tools)
* ["Intel ME Secrets" by Igor Skochisnky](https://recon.cx/2014/slides/Recon%202014%20Skochinsky.pdf)
* [MEAnalyzer](https://github.com/platomav/MEAnalyzer)
* [UnME11](https://github.com/ptresearch/unME11)
( [MEAnalyzer](https://github.com/platomav/MEAnalyzer)

## UEFI

* [Analyzing UEFI BIOSes from an attacker perspective, BHEU 2014](https://www.blackhat.com/docs/eu-14/materials/eu-14-Kovah-Analyzing-UEFI-BIOSes-From-Attacker-And-Defender-Viewpoints.pdf)
* [Using UEFITool](https://www.win-raid.com/t3061f16-Guide-How-to-extract-insert-replace-EFI-BIOS-modules-by-using-the-UEFITool.html)
* [UEFI on osdev.org](https://wiki.osdev.org/UEFI)
* [Matrosov Betraying The BIOS Where The Guardians Of The BIOS Are Failing](https://www.blackhat.com/docs/us-17/wednesday/us-17-Matrosov-Betraying-The-BIOS-Where-The-Guardians-Of-The-BIOS-Are-Failing.pdf)

## LinuxBoot
* [LinuxBoot](https://linuxboot.org)
* [NERF](https://trmm.net/NERF)
* [u-root](https://u-root.tk)
* [coreboot](https://coreboot.org)


## Firmware tools
* [UEFITool](https://github.com/LongSoft/UEFITool)
* [efi-firmware-parser](https://github.com/theopolis/uefi-firmware-parser)
* [FlashRom](https://flashrom.org/Flashrom)
* [coreboot/utils](https://review.coreboot.org/#/admin/projects/coreboot) (clone the repo and see under util)
* [osresearch/linuxboot/bin](https://github.com/osresearch/linuxboot/tree/master/bin)

## UEFI format specs
* [Tianocore EDK II specs](https://github.com/tianocore/tianocore.github.io/wiki/EDK-II-Specifications)
* [Firmware File Format (FFS) specs](https://www.intel.com/content/www/us/en/architecture-and-technology/unified-extensible-firmware-interface/efi-firmware-file-system-specification.html)
* [Trammell Hudson's UEFI notes](https://trmm.net/UEFI_notes)
* [Intel Flash Descriptor talk](http://opensecuritytraining.info/IntroBIOS_files/Day2_02_Advanced%20x86%20-%20BIOS%20and%20SMM%20Internals%20-%20Flash%20Descriptor.pdf)


## LinuxBoot press coverage
* 2017-11-20 (pre-LinuxBoot) https://lwn.net/Articles/738649/
* 2018-01-25 https://www.linuxfoundation.org/blog/system-startup-gets-a-boost-with-new-linuxboot-project/
* 2018-01-25 https://www.phoronix.com/scan.php?page=news_item&px=LinuxBoot-Announced
* 2018-01-26 https://www.linux.com/news/system-startup-gets-boost-new-linuxboot-project
* 2018-01-27 https://lwn.net/Articles/745498/
* 2018-02-02 https://news.ycombinator.com/item?id=16259373
* 2018-02-15 http://www.linuxjournal.com/content/foss-project-spotlight-linuxboot (also on the March issue (for subscribers), http://www.linuxjournal.com/content/it%E2%80%99s-here-march-2018-issue-linux-journal-available-download-now
* 2018-03-07 https://lwn.net/Articles/748586/
* 2018-03-18 http://linuxfr.org/users/vejmarie/journaux/l-epopee-nerf
* 2018-06-28 https://www.phoronix.com/scan.php?page=news_item&px=Watson-Rotundu-Coreboot
* 2018-06-29 https://firmwaresecurity.com/2018/06/29/systemboot-a-linuxboot-distro-that-works-as-a-system-firmware-bootloader-based-on-u-root/
* 2018-02-06 https://www.phoronix.com/scan.php?page=news_item&px=LinuxBoot-2019


## Tech talks
* https://events.linuxfoundation.org/events/elc-openiot-north-america-2018/
* Embedded Linux Conference 2018, https://schd.ws/hosted_files/elciotna18/b7/NERF%20ELC%202018%20Slides.pdf

TODO add the other talks